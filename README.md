<h1>HA Apache (Ansible + Docker)</h1>

O projeto HA_apache cria um ambiente para aplicações PHP, são construidos três 
containers. O script faz toda a instalação do ambiente, a ideia é ter containers 
com Apache e balaceamento de carga com HAproxy, sendo acessado direto pelo browser
pelo endereço ip do servidor.

<h3> Instalação:</h3>

Sistema operacional utlizado: Centos 7

1 -  Primeiro passo:

$ sudo yum install epel-release

2 - Instalação do ansible:

$ sudo yum install ansible


<h3> Arquivos necessários para configurar: </h3>

HA_apache>hosts>hosts: 

necessário mudar o ip do hosts, usuário e senha da máquina que quer fazer o deploy.


<h3> Instalação: </h3>

Na pasta HA_apache utilize o comando:

[user@localhost apache] ansible-playbook -i hosts/hosts main.yml


<h3> Deploy de aplicação: </h3>

O diretório para colocar aplicação:


HA_apache/docker/apache/

/web

/web1

/web2

<h3> Testes: </h3>

Acessando via browser o endereço do servidor: 
url: ip/haproxy_stats
Você poderar ver o balanceamento e realizar testes. 
login: admin
senha: admin

1 - Desativando um containers

<img src="teste1.PNG" >


2 - Todos os containers ativos:

<img src="teste2.PNG" >



 <h3> Autor </h3>

Erick Veiga - @Erickveiga02 - erickveigaf2009@gmail.com

 

